// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

//@URL_API: ruta donde esta alojado el admin y los servicios web
var URL_API = 'http://192.168.12.121/opinion/rest/v1/';  
//@URL_SRC: ruta donde esta alojado un records util para la app
var URL_SRC = 'js/data/'; // 


angular.module('OpinionApp', ['ionic', 'ngCordova', 'LocalStorageModule', 'ionic-datepicker',
                               'OpinionApp.controllers', 'OpinionApp.services',])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

/**
  Declaracion de las constantes de la app, 

*/

.constant('ApiLogin', {
  url: URL_API + 'user/login'
})


.constant('ApiCreate', {
  url: URL_API + 'users'
})


.constant('ApiReset', {
  url: URL_API + 'user/reset-password'
})


.constant('ubicacion', {
  get: function(lat,lng,key){
    var url = 'https://maps.googleapis.com/maps/api/geocode/json';
        if (lat) {
            url += "?latlng={0}".replace("{0}", lat)
        }
         if (lng) {
            url += ",{0}".replace("{0}", lng)
        }
        if (key) {
            url += "&key={0}".replace("{0}", key)
        }
        return url;
  }
})


.constant('SrcPais', {
  templateUrl: URL_SRC + 'pais.json'
})


.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

/**
  Rutas de acceso de la app
*/
   .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
  })

   .state('register', {
      url: '/register',
        templateUrl: 'templates/Register.html',
        controller: 'RegisterCtrl'
  })

   .state('registerTwo', {
      url: '/register/2',
      templateUrl: 'templates/registerTwo.html',
      controller: 'RegisterTwoCtrl'
    })


   .state('recover', {
      url: '/recover',
      templateUrl: 'templates/forgotPass.html',
      controller: 'RecoverCtrl'
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});
