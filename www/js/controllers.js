
angular.module('OpinionApp.controllers', [])

/**
  *Controlador para el login
  *@author Ing. Leonel P. Hernandez M (lhernandez at Dakonon)
  *@email leonelphm@gmail.com
  *@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
  *@date 22/10/2016
  *@version 1.0.0
*/
.controller('LoginCtrl', function($scope, $ionicPopup, $state, LoginService) {
  $scope.data = {};
  $scope.login = function(){
    LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
          if(data.validacion == "ok"){
            $state.go('');     
          }
          else{
            $ionicPopup.alert({
                title: 'Error al entrar!',
                template: data.mensaje + '!'
            });
          }
        }).error(function(data) {
            $ionicPopup.alert({
                title: 'Error al entrar!',
                template: 'Por favor verifica tus credenciales!'
            });
  })
}
})


/**
  *Controlador para Recuperar la contraseña de una cuenta asociada
  *@author Ing. Leonel P. Hernandez M (lhernandez at Dakonon)
  *@email leonelphm@gmail.com
  *@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
  *@date 22/10/2016
  *@version 1.0.0
*/
.controller('RecoverCtrl', function($scope, $ionicPopup, $state, PassResetService) {
 $scope.data = {};
  $scope.recover = function(){
    PassResetService.resetPass($scope.data.email).success(function(data){
      if(data.validacion == "ok"){
        $ionicPopup.alert({
            title: 'Exito !',
            template: data.mensaje + '!'
        });
        $state.go('login');
      }
      else{
        $ionicPopup.alert({
            title: 'Error al recuperar contraseña!',
            template: data.mensaje + '!'
        });
          }
    }).error(function(data) {
        $ionicPopup.alert({
            title: 'Error Desconocido!',
            template: 'Esta intentanto recuperar la contraseña, sin exito!'
        });
      });
  }
})


/**
  *Controlador para el Registrar una cuenta y perfil de usuario
  *@author Ing. Leonel P. Hernandez M (lhernandez at Dakonon)
  *@email leonelphm@gmail.com
  *@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
  *@date 22/10/2016
  *@version 1.0.0
*/
.controller('RegisterCtrl', function($scope, $ionicPopup, $state, 
                                    $cordovaGeolocation, MapaService, PaisServices, 
                                    ObtenerPais, CreateService, ObtenerParam) {
    $scope.data = {};
    var miUbicacion = {};  
    var positionOptions = {timeout: 10000, enableHighAccuracy: true};
    // Se consulta un servio donde se lista todos los paises
    PaisServices.servicesGetPais();
    /*Se debe optimizar la ubicacion ya que no se esta tomando el parametro iso*/
    $cordovaGeolocation.getCurrentPosition(positionOptions).then(function(pos) {
    miUbicacion.lat = pos.coords.latitude;
    miUbicacion.lng = pos.coords.longitude;
    var key = "AIzaSyBdUuS5EuX_XBm4pqp-iJV1HrcY8qpYq7s";
    MapaService.onGet(miUbicacion.lat,miUbicacion.lng,key).success(function(data){
       $scope.data.ubicacion = data.results[0].formatted_address;
       // Varibale a la que se asigna un valor de pais el cual esta asociado en el servidor
       $scope.data.mi_pais = ObtenerPais.getPais(MapaService.getCountry(data.results[0].address_components));
      }).error(function(data){
        $ionicPopup.alert({
                title: 'Error',
                template: 'Verifique sus datos'
            })
       })     
    }),function(error) {
          $ionicPopup.alert({
              title: 'Error de localización',
              template: error.message,
              okType: 'button-assertive'
          });
      }
      $scope.data.mi_pais = "232";
    $scope.enviar = function(){ 
        if(!$scope.data.user){
            $ionicPopup.alert({
                title: 'Error',
                template: 'Introduzca un usuario'
            });
            
          }

          else if(!$scope.data.email){
            $ionicPopup.alert({
                title: 'Error',
                template: 'Introduzca un email'
            });
          }

          else if(!$scope.data.pass){
            $ionicPopup.alert({
                title: 'Error',
                template: 'Introduzca una contraseña'
            });
            
          }

          else if(!$scope.data.passrepeat){
            $ionicPopup.alert({
                title: 'Error',
                template: 'Repita su contraseña'
            });
            
          }
          else if(!$scope.data.mi_pais){
            $ionicPopup.alert({
                title: 'Error',
                template: 'Se debe obtener la ubicacion'
            });
          }
          else{
            ObtenerParam.parametros.registro = $scope.data;
            $state.go('registerTwo');  
          }
        }
})



/**
  *Controlador para el loginregistro de la segunda pantalla del usuario
  *@author Ing. Leonel P. Hernandez M (lhernandez at Dakonon)
  *@email leonelphm@gmail.com
  *@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
  *@date 22/10/2016
  *@version 1.0.0
*/
.controller('RegisterTwoCtrl', function($scope, $ionicPopup, $state, 
                                        $filter, ionicDatePicker, CreateService,
                                        ObtenerParam, CreateService) {
  $scope.data = {}
  $scope.data.date_of_birth = '';
  $scope.data.gender = '';
  var parametros = {};
  var ipObj1 = {
    callback: function (val) {  //Mandatory
      $scope.data.date_of_birth = $filter('date')(new Date(val),'dd/MM/yyyy');
    },
    weeksList: ["D", "L", "M", "M", "J", "V", "S"],
    monthsList: ["Ene", "Feb", "Mar", "Abril", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    from: new Date(1900, 1, 1), 
    to: new Date(2100, 10, 30), 
    inputDate: new Date(),      
    mondayFirst: true,
    closeOnSelect: false,       
    templateType: 'popup'       
  };
  $scope.click = function(){
    ionicDatePicker.openDatePicker(ipObj1);
  }
  $scope.enviar = function (){  
      if($scope.data.date_of_birth == ""){
        $ionicPopup.alert({
            title: 'Error',
            template: 'Introduzca su fecha de nacimiento'
        });
      }
      else if($scope.data.gender == ""){
        $ionicPopup.alert({
            title: 'Error',
            template: 'Introduzca un genero'
        });
      }
      else{
        var edad = '';
        var fech_naci = $scope.data.date_of_birth.split("/")
        var nacimiento = new Date(fech_naci[1] + "/" + fech_naci[0] + "/" + fech_naci[2]); 
        var fecha_actual = new Date();
        // calcula la edad a partir de la fecha actual del dispositivo y de la fecha ingresada
        edad = parseInt((fecha_actual - nacimiento)/365/24/60/60/1000)
        ObtenerParam.parametros.registro.fecha_naci = fech_naci[2] + "-" + fech_naci[1] + "-" + fech_naci[0];
        ObtenerParam.parametros.registro.edad = edad;
        ObtenerParam.parametros.registro.genero = $scope.data.gender;
        parametros = ObtenerParam.parametros.registro;
        CreateService.createUser(parametros).success(function(data){
          if(data.validacion == "ok"){
            $ionicPopup.alert({
                    title: 'Exito !',
                    template: data.mensaje + '!'
                });
            $state.go('login');
          }
          else{
            $ionicPopup.alert({
                title: 'Error al recuperar contraseña!',
                template: data.mensaje + '!'
            });
          }
        }).error(function(data) {
            $ionicPopup.alert({
              title: 'Error Desconocido!',
              template: 'Esta intentanto recuperar la contraseña, sin exito!'
          });
        });
      }
    }
})