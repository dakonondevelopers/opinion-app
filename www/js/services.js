angular.module('OpinionApp.services', [])


/**
  *Servicio para el login
  *@author Ing. Leonel P. Hernandez M (lhernandez at Dakonon)
  *@email leonelphm@gmail.com
  *@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
  *@date 22/10/2016
  *@version 1.0.0
*/
.factory('LoginService', function($q, $http, ApiLogin, localStorageService) {
    return {
      /**
        *loginUser funcion que consulta el servicio web para el logeo del usuario
        *@param name nombre del usuario
        *@param pw password del usuario
        *@return promise respuesta diferida para el controlador
      */
        loginUser: function(name, pw) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            var parametros = JSON.stringify({"username": name, "password": pw})
            $http.post(ApiLogin.url, parametros)
                        .success(function(data) {
                        if(data) {
                            localStorageService.clearAll();
                            localStorageService.set('auth_token', data.auth_token);
                            deferred.resolve(data);
                        } else {
                          console.log(data);
                            deferred.reject(data);
                        }
                       })
                      .error(function(data) {
                          deferred.reject(data);
                      });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    }
})


/**
  *Servicio para Resetear la password del usuario
  *@author Ing. Leonel P. Hernandez M (lhernandez at Dakonon)
  *@email leonelphm@gmail.com
  *@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
  *@date 22/10/2016
  *@version 1.0.0
*/
.factory('PassResetService', function($q, $http, ApiReset){
  return {
      /**
        *resetPass funcion que consulta el servicio web para resetear la password del usuario
        *@param correo email del usuario
        *@return promise respuesta diferida para el controlador
      */
        resetPass: function(correo) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            var parametros = JSON.stringify({"email": correo})
            $http.post(ApiReset.url, parametros).success(function(data) {
                          if(data) {
                            console.log(data);
                            deferred.resolve(data);
                          } else {
                              deferred.reject(data);
                          }
                      })
                        .error(function(data) {
                            deferred.reject(data);
                        });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    }
})


/**
  *Servicio para Crear cuenta y perfil de usuario
  *@author Ing. Leonel P. Hernandez M (lhernandez at Dakonon)
  *@email leonelphm@gmail.com
  *@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
  *@date 22/10/2016
  *@version 1.0.0
*/
.factory('CreateService', function($q, $http, ApiCreate){
  return {
      /**
        *createUser funcion que consulta el servicio web para crear usuario
        *@param parametros objeto con los parametros para crear la cuenta
        *@return promise respuesta diferida para el controlador
      */
        createUser: function(parametros) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            var parametros_create = {
                'edad': parseInt(parametros.edad),
                'email': parametros.email,
                'fecha_nacimiento': parametros.fecha_naci,
                'genero': parametros.genero,
                'pais': parseInt(232), // cambiar por este objeto si se hace la captura de la ubicacion "arametros.mi_pais.idPais"
                'password': parametros.pass,
                'telefono': parametros.telefono,
                'username': parametros.user

            }
            var valores = JSON.stringify({parametros_create})
            $http.post(ApiCreate.url, parametros_create).success(function(data) {
                          if(data) {
                            deferred.resolve(data);
                          } else {
                              deferred.reject(data);
                          }
                      })
                        .error(function(data) {
                            deferred.reject(data);
                        });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    }
})


/**
  *Servicio para obtener ubicacion del dispositivo
  *@author Ing. Leonel P. Hernandez M (lhernandez at Dakonon)
  *@email leonelphm@gmail.com
  *@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
  *@date 22/10/2016
  *@version 1.0.0
*/
.factory('MapaService', function ($http, $q, ubicacion){
    return{
      /**
        *onGet funcion que consulta el servicio de google maps
        *@param lat latitud para la consulta
        *@param lng longitud para la consulta
        *@param key llave publica para usar la api de google
        *@return promise respuesta diferida para el controlador
      */
        onGet: function(lat,lng,key){
        var deferred = $q.defer();
        var promise = deferred.promise;
        var url= ubicacion.get(lat,lng, key);
        $http.get(url).success(function(data){
          if(data){ 
            deferred.resolve(data);
        } 
          else {
            deferred.reject(data);
        }
        }).error(function(data) {
            deferred.reject(data);
        });
        promise.success = function(fn) {
          promise.then(fn);
          return promise;
        }
        promise.error = function(fn) {
                      promise.then(null, fn);
                      return promise;
              }
        return promise;
      },
      /**
        *getCountry funcion que obtiene la direccion del pais
        *@param addrComponents Componentes de la direccion
        *@return addrComponents lista con las caracteristica del pais
      */
      getCountry: function (addrComponents) {
          for (var i = 0; i < addrComponents.length; i++) {
              if (addrComponents[i].types[0] == "country") {
                  return addrComponents[i].short_name;
              }
              if (addrComponents[i].types.length == 2) {
                  if (addrComponents[i].types[0] == "political") {
                      return addrComponents[i].short_name;
                  }
              }
          }
          return false;
        }
    }
})


/**
  *Servicio para obtener la lista de los paises de acuerdo a el registro que se tiene en el sitio web
  *@author Ing. Leonel P. Hernandez M (lhernandez at Dakonon)
  *@email leonelphm@gmail.com
  *@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
  *@date 22/10/2016
  *@version 1.0.0
*/
.factory('PaisServices', function($q, $http, SrcPais, localStorageService){
  return{
      /**
        *servicesGetPais funcion que consulta el recurso json de la api para lista el Records de paises
        *@return promise respuesta diferida para el controlador
      */
    servicesGetPais: function(){
      var deferred = $q.defer();
      var promise = deferred.promise;
      $http.get(SrcPais.templateUrl).success(function(data){
        if(data){
          localStorageService.set('paises', JSON.stringify(data.Records))
          deferred.resolve(data);
        }
        else{
          deferred.resolve(data);
        }
      })
      .error(function(data){
        deferred.reject(data);
      });
      promise.success = function(fn){
        promise.then(fn);
        return promise;
      }
      promise.error = function(fn){
        promise.then(null, fn);
        return promise;
      }
      return promise;
    }
  }
})


/**
  *Servicio para obtener el pais del sitio administrativo, dada la ubicacion por del dispositivo
  *@author Ing. Leonel P. Hernandez M (lhernandez at Dakonon)
  *@email leonelphm@gmail.com
  *@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
  *@date 22/10/2016
  *@version 1.0.0
*/
.factory('ObtenerPais', function($q, $http, localStorageService){
    return{
      /**
        *getPais funcion que obtiene los atributos del pais dado la iso 
        *param pais iso que se obtiene de la ubicacion del dispositivo
        *@return obj_pais objeto pais con los atributos del backend
      */
        getPais: function(pais){
            var paises = localStorageService.get('paises');
            paises = JSON.parse(paises);
            var obj_pais = null;
            for (iso in paises){
                pais = pais.toLowerCase();
                if(paises[iso].iso === pais){
                  obj_pais = paises[iso];
                }
            }
            return obj_pais;
        }
    }
})



/**
  *Servicio que sirve como puente para los controladores
  *@author Ing. Leonel P. Hernandez M (lhernandez at Dakonon)
  *@email leonelphm@gmail.com
  *@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
  *@date 22/10/2016
  *@version 1.0.0
*/
.factory('ObtenerParam', function(){
    /**
      *@return parametros objeto para ser usaudo en los diferentes controladores que interactuan entre si
    */
    return{
        parametros: {} 
    }
})